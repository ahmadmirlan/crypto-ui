class CoinCard {
  late int coinId;
  late String coinLogo;
  late String coinName;
  late double coinValue;
  late double coinPercentage;
  late int coinStatus;

  CoinCard(this.coinId, this.coinLogo, this.coinName, this.coinValue, this.coinPercentage,
      this.coinStatus);
}
