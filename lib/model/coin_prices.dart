import 'package:crypto_app/model/price_data.dart';

class CoinPrices {
  int coinId;
  int coinStatus;
  List<PriceData> prices;

  CoinPrices(this.coinId, this.coinStatus, this.prices);
}
