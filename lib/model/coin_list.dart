class CoinList {
  late int coinId;
  late String coinIcon;
  late String coinName;
  late String coinNickname;
  late double coinValue;
  late double coinPercentage;
  late int coinStatus;

  CoinList(this.coinId, this.coinIcon, this.coinName, this.coinNickname, this.coinValue, this.coinPercentage, this.coinStatus);
}
