import 'package:crypto_app/controller/HomeController.dart';
import 'package:crypto_app/pages/CoinListPages.dart';
import 'package:crypto_app/pages/DashboardPages.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (controller) {
        return Scaffold(
            body: IndexedStack(
              index: controller.tabIndex,
              children: const [
                DashboardScreen(),
                CoinListScreen(),
              ],
            ),
            bottomNavigationBar: BottomNavigationBar(
              showSelectedLabels: false,
              showUnselectedLabels: false,
              unselectedItemColor: Colors.grey,
              selectedItemColor: Colors.greenAccent.shade200,
              onTap: controller.changeTabIndex,
              currentIndex: controller.tabIndex,
              items: const [
                BottomNavigationBarItem(
                    icon: Icon(Icons.home), label: 'Dashboard'),
                BottomNavigationBarItem(icon: Icon(Icons.list), label: 'List'),
                BottomNavigationBarItem(
                    icon: Icon(Icons.pie_chart), label: 'Chart'),
                BottomNavigationBarItem(
                    icon: Icon(Icons.settings), label: 'Setting'),
              ],
            ));
      },
    );
  }
}
