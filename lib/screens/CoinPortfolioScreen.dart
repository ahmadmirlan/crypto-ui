import 'package:crypto_app/controller/HomeController.dart';
import 'package:crypto_app/data/Dummy.dart';
import 'package:crypto_app/model/coin_list.dart';
import 'package:crypto_app/model/coin_prices.dart';
import 'package:crypto_app/model/price_data.dart';
import 'package:crypto_app/widgets/MarketFilterWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class CoinPortfolio extends StatelessWidget {
  const CoinPortfolio({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final oCcy = NumberFormat("#,##0.00", "en_US");
    final String? coinId = Get.parameters['coinId'];
    final int id = int.parse(coinId!);
    final CoinList coin = DummyData.coinList.firstWhere((element) => element.coinId == id);
    final CoinPrices prices = DummyData.priceDataList.firstWhere((element) => element.coinId == id);
    final bool isProfit = coin.coinStatus == 1;

    return GetBuilder<HomeController>(
      builder: (controller) {
        return Scaffold(
            body: Container(
          color: Colors.white,
          padding: const EdgeInsets.only(top: 25),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(onPressed: () {
                    Get.back();
                  }, icon: const Icon(Icons.arrow_back)),
                  const Text('My Portfolio', style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    decoration: TextDecoration.none
                  )),
                  const Icon(Icons.more_vert),
                ],
              ),
              Container(
                margin: const EdgeInsets.only(top: 20),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset('assets/img/coins/${coin.coinIcon}', height: 100, width: 100,),
                  Container(
                    margin: const EdgeInsets.only(top: 15, bottom: 2),
                    child: Text('\$${oCcy.format(coin.coinValue)}',
                        style: const TextStyle(
                            color: Colors.black,
                            decoration: TextDecoration.none,
                            fontWeight: FontWeight.bold,
                            fontSize: 30)),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Chip(
                          backgroundColor: isProfit ? Colors.green.withOpacity(0.2) : Colors.red.withOpacity(0.2),
                          padding: const EdgeInsets.all(0),
                          label: Row(
                            children: [
                              Icon(
                                isProfit ? Icons.arrow_drop_up : Icons.arrow_drop_down,
                                color: isProfit ? Colors.green : Colors.red,
                                size: 30,
                              ),
                              Text(
                                (isProfit ? '+' : '-') + coin.coinPercentage.toString() + '%',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: isProfit ? Colors.green : Colors.red,
                                    fontSize: 15),
                              )
                            ],
                          ))
                    ],
                  )
                ],
              ),
              Container(
                margin: const EdgeInsets.only(top: 20),
              ),
              SizedBox(
                height: 200,
                child: SfCartesianChart(
                  plotAreaBorderWidth: 0,
                  borderWidth: 0,
                  margin: const EdgeInsets.all(0),
                  borderColor: Colors.white,
                  primaryYAxis: NumericAxis(
                    isVisible: false,
                  ),
                  primaryXAxis: NumericAxis(isVisible: false),
                  series: [
                    SplineAreaSeries<PriceData, int>(
                        dataSource: prices.prices,
                        xValueMapper: (PriceData data, _) => data.x,
                        yValueMapper: (PriceData data, _) => data.y,
                        borderDrawMode: BorderDrawMode.top,
                        borderWidth: 3,
                        borderColor: isProfit ? Colors.green : Colors.red,
                        gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomLeft,
                          colors: [
                            isProfit ? Colors.green : Colors.red.shade300,
                            Colors.white,
                          ],
                        ))
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [MarketFilterWidget()],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 15, right: 15),
                margin: const EdgeInsets.only(top: 30, bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    OpenCloseWidget(title: 'Open', value: '153.74'),
                    OpenCloseWidget(title: 'Vol', value: '58.34m'),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 15, right: 15),
                margin: const EdgeInsets.only(top: 10, bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    OpenCloseWidget(title: 'High', value: '154.61'),
                    OpenCloseWidget(title: 'P/E', value: '30.21'),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 15, right: 15),
                margin: const EdgeInsets.only(top: 10, bottom: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    OpenCloseWidget(title: 'Low', value: '120.12'),
                    OpenCloseWidget(title: 'Mkt Cap', value: '2.511'),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 30, left: 15, right: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 160,
                      child: OutlinedButton(
                          onPressed: () {},
                          style: OutlinedButton.styleFrom(
                            side: const BorderSide(width: 1.0, color: Colors.red),
                          ),
                          child: const Text('Sell',
                              style: TextStyle(color: Colors.red))),
                    ),
                    SizedBox(
                      width: 160,
                      child: ElevatedButton(
                          onPressed: () {},
                          child: const Text('Buy'),
                          style: ButtonStyle(
                            backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.green),
                          )),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
      },
    );
  }
}

class OpenCloseWidget extends StatelessWidget {
  final String title;
  final String value;

  const OpenCloseWidget({Key? key, required this.title, required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 150,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(title,
                style: const TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Colors.grey,
                    fontSize: 12)),
            Text(value,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 12)),
          ],
        ));
  }
}
