import 'package:crypto_app/themes/DarkTheme.dart';
import 'package:crypto_app/themes/LigthTheme.dart';
import 'package:flutter/material.dart';

class AppTheme {
  static ThemeData light = lightTheme;
  static ThemeData dark = darkTheme;
}
