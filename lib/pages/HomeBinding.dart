import 'package:crypto_app/controller/HomeController.dart';
import 'package:get/get.dart';

class HomedBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(() => HomeController());
  }
}
