import 'package:crypto_app/data/Dummy.dart';
import 'package:crypto_app/model/coin_list.dart';
import 'package:crypto_app/widgets/CoinFilterWidget.dart';
import 'package:crypto_app/widgets/CoinListWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CoinListScreen extends StatelessWidget {
  const CoinListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<CoinList> coinList = DummyData.coinList;
    return Container(
      padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('Coin Outline',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22)),
              Container(
                decoration: BoxDecoration(
                    color: Colors.greenAccent.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(50)
                ),
                child: IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.search),
                    color: Colors.greenAccent),
              )
            ],
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 20),
          ),
          const CoinFilter(),
          Container(
            margin: const EdgeInsets.only(bottom: 20),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('Market Is Uptrend',
                      style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                  Container(
                    margin: const EdgeInsets.only(top: 5),
                    child: const Text('In the past 24hrs',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                            color: Colors.grey)),
                  ),
                ],
              ),
              Theme(
                  data: Theme.of(context)
                      .copyWith(canvasColor: Colors.transparent),
                  child: Chip(
                      padding: const EdgeInsets.all(0),
                      backgroundColor: Colors.greenAccent.withOpacity(0.2),
                      label: Row(
                        children: const [
                          Icon(Icons.arrow_drop_up,
                              color: Colors.greenAccent, size: 30),
                          Text(
                            '9,7%',
                            style: TextStyle(
                                color: Colors.greenAccent,
                                fontSize: 13,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      )))
            ],
          ),
          Expanded(
              child: GridView.count(
                crossAxisCount: 1,
                crossAxisSpacing: 1,
                mainAxisSpacing: 10,
                childAspectRatio: 6,
                children: List.generate(coinList.length, (index) {
                  return CoinListWidget(coin: coinList[index]);
                }),
              ))
        ],
      ),
    );
  }
}
