import 'package:crypto_app/data/Dummy.dart';
import 'package:crypto_app/model/coin_card.dart';
import 'package:crypto_app/widgets/dashboard/BalanceWidget.dart';
import 'package:crypto_app/widgets/dashboard/CoinCardWidget.dart';
import 'package:crypto_app/widgets/dashboard/DisplayProfileWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<CoinCard> portfolioCoinList = DummyData.portfolioCoinList;
    return Container(
      decoration: BoxDecoration(
        color: Colors.grey.withOpacity(0.3),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding:
            const EdgeInsets.only(left: 20, right: 20, top: 50, bottom: 10),
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomLeft,
                  colors: [
                    Colors.green,
                    Colors.greenAccent,
                  ],
                )),
            child: Column(
              children: [
                const DisplayProfileWidget(),
                const BalanceWidget(),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  padding: const EdgeInsets.all(10),
                  margin: const EdgeInsets.only(top: 50),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: const [
                          Icon(Icons.payments, color: Colors.greenAccent, size: 35),
                          Text('Withdraw',
                              style: TextStyle(
                                  fontSize: 10, fontWeight: FontWeight.w600))
                        ],
                      ),
                      Column(
                        children: const [
                          Icon(Icons.credit_card, color: Colors.greenAccent, size: 35),
                          Text('Deposit',
                              style: TextStyle(
                                  fontSize: 10, fontWeight: FontWeight.w600))
                        ],
                      ),
                      Column(
                        children: const [
                          Icon(Icons.history, color: Colors.greenAccent, size: 35),
                          Text('History',
                              style: TextStyle(
                                  fontSize: 10, fontWeight: FontWeight.w600))
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              children: [

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('My Portfolio',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 13)),
                    TextButton(
                      style: TextButton.styleFrom(
                          textStyle: const TextStyle(
                              fontSize: 10, fontWeight: FontWeight.bold),
                          primary: Colors.greenAccent.shade200),
                      onPressed: () {},
                      child: const Text('See all'),
                    )
                  ],
                )
              ],
            ),
          ),
          Expanded(
              child: GridView.count(
                padding: const EdgeInsets.only(left: 20, right: 20),
                crossAxisCount: 2,
                crossAxisSpacing: 20,
                mainAxisSpacing: 20,
                children: List.generate(portfolioCoinList.length, (index) {
                  final CoinCard coinCard = portfolioCoinList[index];
                  return GestureDetector(
                    onTap: () {
                      Get.toNamed("/coin", parameters: {'coinId': coinCard.coinId.toString()});
                    },
                    child: CoinCardWidget(coinCardPortfolio: coinCard),
                  );
                }),
              ))
        ],
      ),
    );
  }
}
