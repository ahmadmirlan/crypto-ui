import 'package:crypto_app/model/coin_card.dart';
import 'package:crypto_app/model/coin_list.dart';
import 'package:crypto_app/model/coin_prices.dart';
import 'package:crypto_app/model/price_data.dart';

class DummyData {
  static List<CoinCard> portfolioCoinList = [
    CoinCard(0, 'bitcoin.png', 'Bitcoin', 20000, 5.8, 1),
    CoinCard(4, 'ripple.png', 'Ripple', 500, 70.8, 0),
    CoinCard(1, 'ethereum.png', 'Ethereum', 824, 15.8, 1),
    CoinCard(3, 'dogecoin.png', 'Doge', 123, 10.9, 0),
    CoinCard(5, 'dai.png', 'Dai', 100, 5.8, 1),
    CoinCard(7, 'solana.png', 'Solana', 124, 10.8, 1),
  ];

  static List<CoinList> coinList = [
    CoinList(0, 'bitcoin.png', 'Bitcoin', 'BTC', 43610.70, 3.67, 1),
    CoinList(1, 'ethereum.png', 'Ethereum', 'ETH', 1964.69, 2.60, 1),
    CoinList(2, 'binance.png', 'Binance', 'BNB', 439.36, 4.34, 1),
    CoinList(3, 'dogecoin.png', 'Doge Coin', 'DOGE', 0.15, 6.78, 0),
    CoinList(4, 'ripple.png', 'Ripple', 'XRP', 0.79, 0.40, 0),
    CoinList(5, 'dai.png', 'Dai', 'DAI', 58.68, 3.67, 1),
    CoinList(6, 'cardano.png', 'Cardano', 'ADA', 1.09, 2.89, 0),
    CoinList(7, 'solana.png', 'Solana', 'SOL', 115.83, 0.23, 0),
  ];

  static List<CoinPrices> priceDataList = [
    CoinPrices(0, 1, [
      PriceData(1, 40, 3.3),
      PriceData(3, 88, 2.65),
      PriceData(5, 39, 1.99),
      PriceData(7, 90, 3.3),
      PriceData(9, 90, 3.3),
      PriceData(11, 75, 3.3),
      PriceData(13, 70, 3.3),
      PriceData(15, 90, 3.3),
    ]),
    CoinPrices(1, 1, [
      PriceData(1, 40, 3.3),
      PriceData(3, 88, 2.65),
      PriceData(5, 39, 1.99),
      PriceData(7, 90, 3.3),
      PriceData(9, 90, 3.3),
      PriceData(11, 75, 3.3),
      PriceData(13, 70, 3.3),
      PriceData(15, 90, 3.3),
    ]),
    CoinPrices(3, 0, [
      PriceData(1, 40, 3.3),
      PriceData(3, 88, 2.65),
      PriceData(5, 39, 1.99),
      PriceData(7, 90, 3.3),
      PriceData(9, 90, 3.3),
      PriceData(11, 75, 3.3),
      PriceData(13, 70, 3.3),
      PriceData(15, 90, 3.3),
    ]),
    CoinPrices(4, 0, [
      PriceData(1, 70, 3.3),
      PriceData(3, 30, 2.65),
      PriceData(5, 39, 1.99),
      PriceData(7, 32, 3.3),
      PriceData(9, 30, 3.3),
      PriceData(11, 20, 3.3),
      PriceData(13, 30, 3.3),
      PriceData(15, 10, 3.3)
    ]),
    CoinPrices(5, 1, [
      PriceData(1, 40, 3.3),
      PriceData(3, 88, 2.65),
      PriceData(5, 39, 1.99),
      PriceData(7, 90, 3.3),
      PriceData(9, 90, 3.3),
      PriceData(11, 75, 3.3),
      PriceData(13, 70, 3.3),
      PriceData(15, 90, 3.3),
    ]),
    CoinPrices(7, 1, [
      PriceData(1, 40, 3.3),
      PriceData(3, 88, 2.65),
      PriceData(5, 39, 1.99),
      PriceData(7, 90, 3.3),
      PriceData(9, 90, 3.3),
      PriceData(11, 75, 3.3),
      PriceData(13, 70, 3.3),
      PriceData(15, 90, 3.3),
    ])
  ];
}
