import 'package:crypto_app/pages/HomeBinding.dart';
import 'package:crypto_app/routes/AppRoutes.dart';
import 'package:crypto_app/screens/CoinPortfolioScreen.dart';
import 'package:crypto_app/screens/HomeScreen.dart';
import 'package:get/get.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.DASHBOARD,
        page: () => const HomeScreen(),
        binding: HomedBinding(),
        transition: Transition.rightToLeft),
    GetPage(
        name: AppRoutes.COIN_PORTFOLIO,
        page: () => const CoinPortfolio(),
        transition: Transition.leftToRight)
  ];
}
