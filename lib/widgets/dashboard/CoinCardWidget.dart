import 'package:crypto_app/model/coin_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CoinCardWidget extends StatelessWidget {
  final CoinCard coinCardPortfolio;
  const CoinCardWidget({Key? key, required this.coinCardPortfolio}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool isProfit =  coinCardPortfolio.coinStatus == 1;
    final oCcy = NumberFormat("#,##0.00", "en_US");
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/img/coins/' + coinCardPortfolio.coinLogo,
            width: 60,
            height: 60,
          ),
          Container(
            margin: const EdgeInsets.only(top: 5),
            child: Text(coinCardPortfolio.coinName,
                style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    letterSpacing: 1)),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('\$' + oCcy.format(coinCardPortfolio.coinValue),
                  style: const TextStyle(
                      fontSize: 12,
                      color: Colors.grey,
                      fontWeight: FontWeight.bold)),
              Container(
                padding: const EdgeInsets.all(2),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: isProfit ? Colors.greenAccent.withOpacity(0.2) : Colors.red.withOpacity(0.2)),
                child: Icon(isProfit ? Icons.trending_up : Icons.trending_down, color: isProfit ? Colors.green : Colors.red , size: 14),
              )
            ],
          ),
          Text( (isProfit ? '+' : '-') + coinCardPortfolio.coinPercentage.toString() + '%',
              style: TextStyle(
                  color: isProfit ? Colors.green.shade300 : Colors.red.shade200,
                  fontSize: 12,
                  fontWeight: FontWeight.bold))
        ],
      ),
    );
  }
}
