import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BalanceWidget extends StatelessWidget {
  const BalanceWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'My Balance',
                style: TextStyle(
                    color: Colors.white54,
                    fontSize: 15,
                    fontWeight: FontWeight.w700),
              ),
              Container(margin: const EdgeInsets.only(top: 10)),
              const Text(
                '\$35,410,00',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 25),
              ),
              Container(margin: const EdgeInsets.only(top: 10)),
              Row(
                children: [
                  const Icon(Icons.moving, color: Colors.white, size: 18),
                  Container(margin: const EdgeInsets.only(left: 5)),
                  const Text('\$1,896',
                      style: TextStyle(
                          fontWeight: FontWeight.w400, color: Colors.white)),
                  Container(margin: const EdgeInsets.only(left: 10)),
                  const Text("Today's Profit",
                      style: TextStyle(
                          fontSize: 13,
                          color: Colors.white54,
                          fontWeight: FontWeight.bold)),
                ],
              ),
            ],
          ),
          Theme(
              data: Theme.of(context).copyWith(canvasColor: Colors.transparent),
              child: Chip(
                  backgroundColor: Colors.white.withOpacity(0.1),
                  label: Row(
                    children: const [
                      Icon(Icons.arrow_drop_up, color: Colors.white, size: 30,),
                      Text(
                        '20%',
                        style: TextStyle(color: Colors.white, fontSize: 15),
                      )
                    ],
                  )))
        ],
      ),
    );
  }
}
