import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DisplayProfileWidget extends StatelessWidget {
  const DisplayProfileWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'Welcome Back,',
              style: TextStyle(
                color: Colors.white60,
                fontSize: 25,
                fontWeight: FontWeight.w400,
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 5),
            ),
            const Text('Mirlan',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.normal))
          ],
        ),
        Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50), color: Colors.white.withOpacity(0.1)),
            padding: const EdgeInsets.all(5),
            child: Center(
              child: Image.asset('assets/img/avatar01.webp',
                  height: 40, width: 40),
            )),
      ],
    );
  }
}
