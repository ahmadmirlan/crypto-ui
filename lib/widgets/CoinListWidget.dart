import 'package:crypto_app/model/coin_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CoinListWidget extends StatelessWidget {
  final CoinList coin;

  const CoinListWidget({Key? key, required this.coin}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isProfit = coin.coinStatus == 1;
    final oCcy = NumberFormat("#,##0.00", "en_US");
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Image.asset('assets/img/coins/${coin.coinIcon}',
                  height: 40, width: 40),
              Container(
                margin: const EdgeInsets.only(left: 10),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(coin.coinName,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 15)),
                  Container(
                    margin: const EdgeInsets.only(top: 1.5, bottom: 1.5),
                  ),
                  Text(coin.coinNickname,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                          color: Colors.grey)),
                ],
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text('\$' + oCcy.format(coin.coinValue),
                  style: const TextStyle(fontWeight: FontWeight.bold)),
              Row(
                children: [
                  Icon(isProfit ? Icons.arrow_drop_up : Icons.arrow_drop_down,
                      color: isProfit ? Colors.greenAccent : Colors.red,
                      size: 25),
                  Text(
                    (isProfit ? '+' : '-') +
                        coin.coinPercentage.toString() +
                        '%',
                    style: TextStyle(
                        color: isProfit ? Colors.greenAccent : Colors.red,
                        fontSize: 12,
                        fontWeight: FontWeight.bold),
                  )
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
