import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ActorFilterEntry {
  const ActorFilterEntry(this.name, this.initials);

  final String name;
  final String initials;
}

class MarketFilterWidget extends StatefulWidget {
  const MarketFilterWidget({Key? key}) : super(key: key);

  @override
  State createState() => MarketFilterState();
}

class MarketFilterState extends State<MarketFilterWidget> {
  final List<ActorFilterEntry> _cast = <ActorFilterEntry>[
    const ActorFilterEntry('1D', '1D'),
    const ActorFilterEntry('7D', '7D'),
    const ActorFilterEntry('1M', '1M'),
    const ActorFilterEntry('1Y', '1Y'),
    const ActorFilterEntry('5Y', '5Y'),
    const ActorFilterEntry('All', 'All'),
  ];
  String _filters = '7D';

  Iterable<Widget> get actorWidgets {
    return _cast.map((ActorFilterEntry actor) {
      return Padding(
        padding: const EdgeInsets.all(4.0),
        child: Theme(
          data: Theme.of(context).copyWith(canvasColor: Colors.white),
          child: ChoiceChip(
            label: Text(actor.name, style: TextStyle(
                color: _filters == actor.name ? Colors.green.shade400 : Colors.grey
            )),
            selected: _filters == actor.name,
            backgroundColor: Colors.white,
            selectedColor: Colors.greenAccent.withOpacity(0.2),
            onSelected: (bool value) {
              setState(() {
                if (value) {
                  _filters = actor.name;
                }
              });
            },
          ),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: actorWidgets.toList(),
    );
  }
}
