import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ActorFilterEntry {
  const ActorFilterEntry(this.name, this.initials);

  final String name;
  final String initials;
}

class CoinFilter extends StatefulWidget {
  const CoinFilter({Key? key}) : super(key: key);

  @override
  State createState() => CoinFilterState();
}

class CoinFilterState extends State<CoinFilter> {
  final List<ActorFilterEntry> _cast = <ActorFilterEntry>[
    const ActorFilterEntry('All', 'ALL'),
    const ActorFilterEntry('24h', '24H'),
    const ActorFilterEntry('Top 50', 'T50'),
    const ActorFilterEntry('Market Cap', 'MC'),
  ];
  String _filters = '24h';

  Iterable<Widget> get actorWidgets {
    return _cast.map((ActorFilterEntry actor) {
      return Padding(
        padding: const EdgeInsets.all(4.0),
        child: Theme(
          data: Theme.of(context).copyWith(canvasColor: Colors.transparent),
          child: ChoiceChip(
            label: Text(actor.name, style: TextStyle(
              color: _filters == actor.name ? Colors.greenAccent.shade200 : Colors.grey
            )),
            selected: _filters == actor.name,
            selectedColor: Colors.greenAccent.withOpacity(0.2),
            onSelected: (bool value) {
              setState(() {
                if (value) {
                  _filters = actor.name;
                }
              });
            },
          ),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: actorWidgets.toList(),
    );
  }
}
